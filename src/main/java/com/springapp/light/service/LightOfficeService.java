/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapp.light.service;



import com.springapp.light.domain.LightOffice;
import com.springapp.light.domain.LightOfficePower;
import com.springapp.light.domain.LightOfficeSize;
import com.springapp.light.domain.LightOfficeType;


import java.util.List;
import org.springframework.web.multipart.MultipartFile;


public interface LightOfficeService {
    

    String[] listImage(String path);
 
    void uploadLightOffice(String path, MultipartFile[] file);
    void uploadImagesLight(String path, MultipartFile[] images);

    LightOffice getLightByUrl(String url);
    LightOffice getLightById(String id);

    List<LightOffice> getListLighByIds(String ids);
    List<LightOffice> getListLightFromSearch(String word);
    List<LightOffice> getListLightOffice();
    List<LightOffice> getListLightOffice(String emergency, String powers, String size, String type);


    List<LightOfficePower> getListLightOfficePower();
    List<LightOfficeSize> getListLightOfficeSize();
    List<LightOfficeType> getListLightOfficeType();

    void renewFiltersLight();

}

